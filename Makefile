all:
	for i in $$(find . -maxdepth 1 -type d -not -name .git -not -name target -not -name . | cut -c3-); do \
		$(MAKE) target/$${i}.pdf; \
	done

.PHONY: docker
docker:
	docker build . -t pocketmod:latest
	docker run --rm -v $$(pwd):/opt/src/ -w /opt/src pocketmod:latest make -B target/_TEMPLATE.pdf

target/%.pdf: target/%/ target/%/background.pdf target/%/pocketmod.pdf
	pdftk target/$*/pocketmod.pdf background target/$*/background.pdf output $@

target/%/:
	-mkdir -p $@

target/%/background.pdf:
	inkscape -z --export-pdf=$@ $*/background.svg

target/%/pocketmod_base.pdf: target/%/pages.pdf
	pdfjam -o /dev/stdout "$<" "1-8" --outfile $@

target/%/pocketmod.pdf: target/%/pocketmod_base.pdf
	# Taken from `pdfjam-pocketmod`
	pdfjam -o /dev/stdout --angle 180  "$<" '1,8,7,6' \
		| pdfjam --nup 4x2 --landscape -o /dev/stdout /dev/stdin - "$<" '2,3,4,5' \
		| pdfjam --landscape --frame true --outfile "$@" /dev/stdin -

target/%/pages.pdf: %/page_1.pdf %/page_2.pdf %/page_3.pdf %/page_4.pdf %/page_5.pdf %/page_6.pdf %/page_7.pdf %/page_8.pdf
	pdftk $^ cat output $@

%/page_1.pdf:
	inkscape -z --export-pdf=$@ $*/page_1.svg
%/page_2.pdf:
	inkscape -z --export-pdf=$@ $*/page_2.svg
%/page_3.pdf:
	inkscape -z --export-pdf=$@ $*/page_3.svg
%/page_4.pdf:
	inkscape -z --export-pdf=$@ $*/page_4.svg
%/page_5.pdf:
	inkscape -z --export-pdf=$@ $*/page_5.svg
%/page_6.pdf:
	inkscape -z --export-pdf=$@ $*/page_6.svg
%/page_7.pdf:
	inkscape -z --export-pdf=$@ $*/page_7.svg
%/page_8.pdf:
	inkscape -z --export-pdf=$@ $*/page_8.svg

