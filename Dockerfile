FROM ubuntu:disco

RUN apt-get update && apt-get install -y --no-install-recommends \
  inkscape \
  pdftk \
  texlive-extra-utils \
  texlive-latex-recommended \
  build-essential

COPY *.ttf /usr/share/fonts/
RUN fc-cache -fv